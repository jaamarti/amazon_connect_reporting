import json
import boto3
import time

query = """SELECT * FROM {0} where from_iso8601_timestamp(initiationtimestamp) > (current_timestamp - interval '12' hour);"""

db_name = 'connectanalitic_db'
output_location = 's3://ctg-amazonconnect-athena-reports/ctr-records/'
table_name = 'ctr'
catalog = 'AwsDataCatalog'

def lambda_handler(event, context):
    
    total_iterations = 20    
    
    client = boto3.client('athena')
    query_start = client.start_query_execution(
        QueryString= query.format(table_name),
        QueryExecutionContext={
            'Database': db_name,
            'Catalog' : catalog
        },
        ResultConfiguration={
            'OutputLocation': output_location
        }
    )

    query_id = query_start['QueryExecutionId']
    
    while (total_iterations > 0):
        total_iterations -=1
        
        queryExecutionObject = client.get_query_execution(QueryExecutionId = query_id)
        
        request_id = queryExecutionObject['ResponseMetadata']['RequestId']
        status = queryExecutionObject['QueryExecution']['Status']['State']
        
        if status == "FAILED" or status == "CANCELLED":
            raise Exception("Query unsuccessfull with status:{} for requestId: {}")
        elif status == "SUCCEEDED":
            results = client.get_query_results(QueryExecutionId=query_id)
            rows = results['ResultSet']['Rows']
        else:
            time.sleep(2)
    
    data = [ data_row["Data"][0]["VarCharValue"] for data_row in rows[1:] ]
    
    responseObj = {}
    responseObj['requestId'] = queryExecutionObject['ResponseMetadata']['RequestId']
    responseObj['names'] = data
    print(responseObj)
    return responseObj
